/**
* @abstract Orders source file for billing application
*/

/**
	Billing in C lets you create inventory, orders for a store or shop
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "bll.h"

BLLORD *create_bll_ford(BLL *Bll) { // Fresh orders when first in Bll struct is found NULL
	BLLORD *ord;
	time_t ticks;

	ord = (BLLORD *) malloc(sizeof(BLLORD));
	if(ord == NULL) {
		printf("Creation of first order failed!\n");
		return NULL;
	}

	ticks = time(NULL);
	ord->cust.name[0] = '\0';
	ord->total = 0.0;
	ord->timestamp = (long) ticks;
	ord->firstorditem = NULL;
	ord->currorditem = NULL;

	Bll->firstord = Bll->currord = ord;
	ord->next = NULL;
	return ord;
}

BLLORD *add_bll_ord(BLL *Bll) {
	BLLORD *ord;
	time_t ticks;

	if(Bll->firstord==NULL) {
		ord = create_bll_ford(Bll);
		return ord; // Just memory
	}

	ord = (BLLORD *) malloc(sizeof(BLLORD));
	if(!ord) {
		printf("Order creation failed!\n");
		return NULL;
	}

	ticks = time(NULL);
	ord->cust.name[0] = '\0';
	ord->total = 0.0;
	ord->timestamp = (long) ticks;
	ord->firstorditem = NULL;
	ord->currorditem = NULL;

	Bll->currord->next = ord;
	Bll->currord = ord;

	ord->next = NULL;
	return ord;
}

BLLORDITM *create_bll_forditem(BLLORD *ord) { // Fresh orders when first in Bll struct is found NULL
	BLLORDITM *orditem;

	orditem = (BLLORDITM *) malloc(sizeof(BLLORDITM));
	if(orditem == NULL) {
		printf("Creation of first order-item failed!\n");
		return NULL;
	}

	orditem->next = NULL;
	ord->firstorditem = ord->currorditem = orditem;
	return orditem;
}

BLLORDITM *add_bll_orditem(BLLORD *ord) {
	BLLORDITM *orditem;

	if(ord->firstorditem==NULL) {
		orditem = create_bll_forditem(ord);
		return orditem; // Just memory
	}

	orditem = (BLLORDITM *) malloc(sizeof(BLLORDITM));
	if(!orditem) {
		printf("Order item creation failed!\n");
		return NULL;
	}

	orditem->next = NULL;
	ord->currorditem->next = orditem;
	ord->currorditem = orditem;

	return orditem;
}

int scan_bll_ord(BLLORD *ord, char *line) {
	char *token = NULL;
	char delim[] = ";";
	char itemstr[BLL_MIN_INPUT];
	char itemdelim[] = ":";
	long *itemid = NULL;
	long *nextid = NULL;

	BLLORDITM *orditem = NULL;
	BLLCUST *cust;
	int itemindex = 0;
	int complete_flag = FALSE;

	token = strtok(line, delim);
	if(token!=NULL) {
		ord->id = atol(token);
		token = strtok(NULL, delim);
		if(token!=NULL) { // We got ids separated by colon (:)
			strcpy(ord->cust.name, token);
			token = strtok(NULL, delim);
			if(token!=NULL) {
				strcpy(itemstr, token); // Got the item ids
				token = strtok(NULL, delim);
				if(token!=NULL) {
					ord->total = atof(token);
					token = strtok(NULL, delim);
					if(token!=NULL) {
						ord->timestamp = atol(token);
						complete_flag = TRUE;
					}
				}
			}
		}

		if(strlen(itemstr)>0) { // Items are found
			token = strtok(itemstr, itemdelim);
			if(token!=NULL) {
//				ord->itemids[itemindex] = (long *) malloc(sizeof(long));
//				*(ord->itemids[itemindex]) = atol(token);
				orditem = add_bll_orditem(ord);
				orditem->id = atol(token);
				while((token=strtok(NULL, itemdelim))!=NULL) {
//					++itemindex;
//					ord->itemids[itemindex] = (long *) malloc(sizeof(long));
//					itemid = ord->itemids[itemindex];
//					*(itemid) = atol(token); // Set value
					orditem = add_bll_orditem(ord);
					orditem->id = atol(token);
				}
			}
		} else complete_flag = FALSE;
	}

	return complete_flag;
}

int init_bll_ord(BLL *Bll) {
	int count = 0;

	const char *filename = BLL_ORDFILE;
	FILE *fp;
	char line[BLL_MAX_LINE];
	BLLORD *ord;

	fp = fopen(filename, "r");
	if(fp) {
		while((fgets(line, BLL_MAX_LINE, fp))!=NULL) {
			ord = add_bll_ord(Bll);
			if(ord) {
				if((scan_bll_ord(ord, line))==TRUE)
					count++;
			}
		}
		fclose(fp);
	}
	Bll->ordercount = count;
	return count;
}

int save_bll_ord(BLLORD *ord) {
	int ret = FALSE;

	char *filename = BLL_ORDFILE;
	FILE *fp;
	char line[BLL_MAX_LINE];
	char items[BLL_MIN_INPUT];
	int i = 0; // Loop through the itemsids array
	char buf[BLL_MIN_INPUT];
	time_t ticks;
	BLLORDITM *orditem, *nextorditem;
	BLLCUST *cust;

	fp = fopen(filename, "a+");
	if(fp) {
		/** Build the item ids string */
		items[0] = '\0'; // Clean up the string
		cust = &ord->cust;
		for(orditem=ord->firstorditem; orditem; orditem=nextorditem) {
			sprintf(buf, "%ld", orditem->id);
			strcat(items, buf);
			if(orditem->next) { // Leave first id
				strcat(items, ":");
			}
			nextorditem = orditem->next;
			++i;
		}

		ticks = time(NULL);
		srand((long)ticks / ((long)getpid()/2));
		fprintf(fp, "%ld;%s;%s;%.2lf;%ld\n", ord->id, cust->name, items, ord->total, ord->timestamp);
		printf("Order saved successfully..\n");
		ret = TRUE;
		fclose(fp);
	}

	return ret;
}

int save_bll_ords(BLL *Bll) {
	int ret = FALSE;

	char *filename = BLL_ORDFILE;
	FILE *fp;
	char items[BLL_MIN_INPUT];
	char buf[BLL_MIN_INPUT];
	BLLORD *ord, *nextord;
	BLLORDITM *orditem, *nextorditem;
	BLLCUST *cust;

	fp = fopen(filename, "w");
	if(fp) {
		for(ord=Bll->firstord; ord; ord=nextord) {
			items[0] = '\0';
			cust = &ord->cust;
			for(orditem=ord->firstorditem; orditem; orditem=nextorditem) {
				sprintf(buf, "%ld", orditem->id);
				strcat(items, buf);
				if(orditem->next)
					strcat(items, ":");
				nextorditem = orditem->next;
			}

			fprintf(fp, "%ld;%s;%s;%.2lf;%ld\n", ord->id, cust->name, items, ord->total, ord->timestamp);
			nextord = ord->next;
		}

		fclose(fp);
	}

	return ret;
}

BLLORD *get_bll_ord(BLL *Bll, long id) {
	BLLORD *ord, *next;
	ord = next = NULL;

	for(ord=Bll->firstord; ord; ord=next) {
		if(ord->id == id)
			break;
		next = ord->next;
	}

	return ord;
}

int del_bll_ord(BLL *Bll, long id) {
	int ret = FALSE;
	BLLORD *ord, *prev, *next;
	BLLORD *delord = NULL;

	for(ord=Bll->firstord; ord; ord=next) {
		if(ord->id==id) {
			delord=ord;
			break;
		} else prev=ord;

		next = ord->next;
	}

	if(prev!=NULL && delord!=NULL) {
		next = delord->next; // Setting up which is next for the previous item
		prev->next = next;

		if(delord==Bll->currord) // Last order
			Bll->currord = prev; // Previous is last

		if(delord==Bll->firstord) // First order
			Bll->firstord = next; // Next would be first now

		free(delord);
		ret = TRUE;
	}

	return ret;
}

void del_bll_orditems(BLLORD *ord) {
	BLLORDITM *orditem, *nextorditem;

	for(orditem=ord->firstorditem; orditem; orditem=nextorditem) {
		nextorditem = orditem->next;
		free(orditem);
	}

	ord->firstorditem = ord->currorditem = NULL;
}

void show_bll_ord(BLL *Bll) {
	BLLORD *ord, *next;
	BLLORDITM *orditem, *orditemnext;
	int i = 0;
	int count = 0;
	printf("Displaying orders..\n");

	for(ord=Bll->firstord; ord; ord=next) {
		printf("ID: %ld; Customer: %s", ord->id, ord->cust.name);
		printf("; Items: ");
/*		while(ord->itemids[i]) {
			printf("%ld:", ord->itemids[i]);
			++i;
		}
*/
		for(orditem=ord->firstorditem; orditem; orditem=orditemnext) {
			if(orditem->next)
				printf("%ld:", orditem->id);
			else
				printf("%ld", orditem->id);
			orditemnext = orditem->next;
			++i;
		}

		printf("; Total: %.2lf", ord->total); // Double
		printf("; Timestamp: %ld\n", ord->timestamp);
		++count;
		next = ord->next;
	}

	if(count==0)
		printf("No orders created yet..\n");
}

int bll_ord_exists(long id) { // Verify if order with supplied ID exists in list
	int exists = TRUE;

	return exists;
}
