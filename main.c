// main source file for billing application

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// Include our header
#include "bll.h"
// Include generic functions
#include "fnc.h"

void bll_cleanup(BLL *Bll) {
	BLLITM *itm, *itmnext;
	BLLORD *ord, *ordnext;
	BLLORDITM *orditem, *orditemnext;
	int i;

	/** Clean up items */
	for(itm=Bll->firstitem; itm; itm=itmnext) {
		itmnext = itm->next;
		free(itm);
	}

	for(ord=Bll->firstord; ord; ord=ordnext) {
		i = 0;

		/** Clean up itemids for order */
/*		while((ord->itemids[i])) {
			free(&ord->itemids[i]);
			++i;
		}
*/
		for(orditem=ord->firstorditem; orditem; orditem=orditemnext) {
			orditemnext = orditem->next;
			free(orditem);
			++i;
		}

		ordnext = ord->next;
		free(ord);
	}

}

int main(int argc, char *argv[], char *env[]) {
	BLL Bll;
	BLLITM *itm;
	BLLORD *ord;
	char entity[BLL_MIN_INPUT];
	char op;

/*
	char gpl[] = "Billing-C  Copyright (C) 2016  vizcreations\n"
					"This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n"
					"This is free software, and you are welcome to redistribute it\n"
					"under certain conditions; type `show c' for details.\n";
*/

//	printf("%s\n", gpl); // Print the license and name of program

	/** Initialize physical data into memory */
	printf("Billing in Linux. Copyright vizcreations 2016\n\n");
	Bll.firstitem = Bll.curritem = NULL;
	Bll.firstord = Bll.currord = NULL;

	Bll.itemcount = 0;
	Bll.ordercount = 0;

	init_bll_cfg(&Bll);
	Bll.ordercount = init_bll_invt(&Bll);
	Bll.itemcount = init_bll_ord(&Bll);

	op = 's';
	if(argc > 1) {

		/**
		* arg-1 is the entity with which we work
		* arg-2 is the action or the work
		* arg-3 and so on are parameters
		*/
		strcpy(entity, argv[1]);

		if(argc > 2) { // Some action defined

			if(argv[2][0] == '-'
			&& argv[2][1] != 0) // Not terminator
				op = argv[2][1];

			switch(op) {
				case 'n':
					{

						char itemstr[BLL_MIN_INPUT];
						char *token;
						char itmdelim[] = ":";
						int itemindex = 0;
						time_t ticks;
						double total = 0.0;

						BLLITM *item;
						BLLORDITM *orditem;
						if(strcmp(entity,"invt")==0) { // Add item
							if(argc>6) {
								++Bll.itemcount; // Increment from previous id
								itm = add_bll_item(&Bll);
								itm->id = Bll.itemcount;
								strcpy(itm->name, argv[3]);
								strcpy(itm->desc, argv[4]);
								itm->in_stock = 1;
								itm->weight = atof(argv[5]);
								itm->price = atof(argv[6]);
								itm->mrp = atof(argv[7]);

								if((save_bll_item(itm))==FALSE)
									--Bll.itemcount; // Rollback item

								show_bll_invt(&Bll);
							} else {
								printf("Usage: %s invt -n <name> <desc> <weight> "
											"<price> <mrp>\n", argv[0]);
							}
						} else if(strcmp(entity,"ord")==0) {
							if(argc>3) {
								++Bll.ordercount;
								ord = add_bll_ord(&Bll);
								ord->id = Bll.ordercount;
								ticks = time(NULL);
								strcpy(ord->cust.name, argv[3]);

								itemstr[0] = '\0';
								strcpy(itemstr, argv[4]);
								token = strtok(itemstr, itmdelim);
/*								ord->itemids[itemindex] = (long) malloc(sizeof(long));
								ord->itemids[itemindex] = atol(token);
*/

								if(token!=NULL) {
									orditem = add_bll_orditem(ord);
									orditem->id = atol(token);
									if((bll_item_exists(&Bll, orditem->id))==FALSE) 
										goto fail;
									item = get_bll_item(&Bll, orditem->id);
									total = 0.0;
									total += item->price;
									while((token=strtok(NULL, itmdelim))!=NULL) {
										++itemindex;
//										ord->itemids[itemindex] = 
//											(long) malloc(sizeof(long));
//										itemid = &ord->itemids[itemindex];
//										*itemid = atol(token); // Set value
										orditem = add_bll_orditem(ord);
										orditem->id = atol(token);
										if((bll_item_exists(&Bll, orditem->id))
											==FALSE
										) goto fail;
										item = get_bll_item(&Bll, orditem->id);
										total += item->price;
									}
								}

								ord->total = total;
								ord->timestamp = (long) ticks;

								if((save_bll_ord(ord))==FALSE)
									--Bll.ordercount;

								show_bll_ord(&Bll);

							} else printf("Usage: %s %s %s <customer-name> <item:id:s>\n", 
											argv[0], argv[1], argv[2]);
						}
					}
					break;
				case 'e':
					{

						char itemstr[BLL_MIN_INPUT];
						char *token;
						char itmdelim[] = ":";
						int itemindex = 0;
						long itemid, ordid;
						time_t ticks;
						double total = 0.0;

						BLLORDITM *orditem;
						BLLITM *item;
						if(strcmp(entity,"invt")==0) { // Edit

							if(argc>8) {
								itemid = atol(argv[3]);
								if((bll_item_exists(&Bll, itemid))==FALSE) goto fail;
								itm = get_bll_item(&Bll, itemid);
								itm->id = itemid;
								strcpy(itm->name, argv[4]);
								strcpy(itm->desc, argv[5]);
								itm->in_stock = 1;
								itm->weight = atof(argv[6]);
								itm->price = atof(argv[7]);
								itm->mrp = atof(argv[8]);

								if((save_bll_items(&Bll))==FALSE) ;

								show_bll_invt(&Bll);
							} else {
								printf("Usage: %s %s %s <item-id> <name> <desc> "
										"<weight> <price> <mrp>\n",
											argv[0], argv[1], argv[2]);
							}
						} else if(strcmp(entity,"ord")==0) {
							if(argc>5) {
								ordid = atol(argv[3]);
								ord = get_bll_ord(&Bll, ordid);
								ord->id = ordid;
								ticks = time(NULL);
								strcpy(ord->cust.name, argv[4]);

								itemstr[0] = '\0';
								strcpy(itemstr, argv[5]);
								token = strtok(itemstr, itmdelim);
/*								ord->itemids[itemindex] = (long) malloc(sizeof(long));
								ord->itemids[itemindex] = atol(token);
*/

								del_bll_orditems(ord);
								if(token!=NULL) {
									orditem = add_bll_orditem(ord);
									orditem->id = atol(token);
									if((bll_item_exists(&Bll, orditem->id))==FALSE) 
										goto fail;

									item = get_bll_item(&Bll, orditem->id);
									total = 0.0;
									total += item->price;
									while((token=strtok(NULL, itmdelim))!=NULL) {
										++itemindex;
//										ord->itemids[itemindex] = 
//											(long) malloc(sizeof(long));
//										itemid = &ord->itemids[itemindex];
//										*itemid = atol(token); // Set value
										orditem = add_bll_orditem(ord);
										orditem->id = atol(token);
										if((bll_item_exists(&Bll, orditem->id))
											==FALSE
										) goto fail;
										item = get_bll_item(&Bll, orditem->id);
										total += item->price;
									}
								}

								ord->total = total;
								ord->timestamp = (long) ticks;

								if((save_bll_ords(&Bll))==FALSE) ;

								show_bll_ord(&Bll);
							} else printf("Usage: %s %s %s <orderid> <customer-name> "
										"<item:id:s>\n",
											argv[0], argv[1], argv[2]);
						}
					}
					break;
				case 'm': // Config modify
					{
						if(strcmp(entity,"cfg")==0) {
							char name[BLL_MIN_INPUT];
							char regid[BLL_MIN_INPUT];
							char addr[BLL_MAX_INPUT];
							char estd[BLL_MIN_INPUT];

							double service_tax;
							double vat_tax;
							BLLCFG *cfg;

							if(argc>8) {
								cfg = &Bll.cfg;

								strcpy(name, argv[3]);
								strcpy(regid, argv[4]);
								strcpy(addr, argv[5]);
								strcpy(estd, argv[6]);

								service_tax = atof(argv[7]);
								vat_tax = atof(argv[8]);

								strcpy(cfg->store_name, name);
								strcpy(cfg->store_reg_id, regid);
								strcpy(cfg->store_addr, addr);
								strcpy(cfg->store_estd, estd);

								cfg->service_tax = service_tax;
								cfg->vat_tax = vat_tax;

								save_bll_cfg(cfg);

								show_bll_cfg(&Bll);
							} else printf("Usage: %s %s %s <name> <regid> <addr> <estd> "
									" <service_tax> <vat_tax>\n",
									argv[0], argv[1], argv[2]);
						}
					}
					break;
				case 'd': // Delete from system
					{
						if(strcmp(entity,"invt")==0) {
							long id;

							if(argc>3) {
								id = atol(argv[3]); // 4th argument
								if((del_bll_item(&Bll, id))==TRUE)
									printf("Item <%ld> deleted successfully..\n", 
										id);

								save_bll_items(&Bll);
							} else printf("Usage: %s %s %s <itemid>\n", 
										argv[0], argv[1], argv[2]);

						} else if(strcmp(entity,"ord")==0) {
							long id;

							if(argc>3) {
								id = atol(argv[3]);
								if((del_bll_ord(&Bll, id))==TRUE)
									printf("Order <%ld> deleted successfully..\n", 
													id);
								save_bll_ords(&Bll);
							} else printf("Usage: %s %s %s <orderid>\n", 
											argv[0], argv[1], argv[2]);
						}
					}
					break;
				case 's': // show
				default:

					if(strcmp(entity,"invt")==0) // Display inventory
						show_bll_invt(&Bll);
					else if(strcmp(entity,"ord")==0)
						show_bll_ord(&Bll);
					else if(strcmp(entity,"cfg")==0)
						show_bll_cfg(&Bll);

					break;
			}
		} else printf("Usage: %s %s <options>\n", argv[0], argv[1]);
	} else printf("Usage: %s <invt/ord/cfg> <-n/-e/-s>\n", argv[0]);

	bll_cleanup(&Bll);

	return 0;

fail:
	printf("Technical problem processing your request!\n");
	bll_cleanup(&Bll);
	return 1;
}
