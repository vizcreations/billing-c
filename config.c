/* Configuration module source file */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bll.h"
#include "fnc.h"

void init_bll_cfg(BLL *Bll) {
	BLLCFG *cfg;
	char *filename = BLL_CFGFILE;
	FILE *fp;
	char line[BLL_MAX_LINE];

	cfg = &Bll->cfg;

	fp = fopen(filename, "r");
	if(fp) {
		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			strcpy(cfg->store_name, line);
		}
		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			strcpy(cfg->store_reg_id, line);
		}
		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			strcpy(cfg->store_addr, line);
		}
		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			strcpy(cfg->store_estd, line);
		}

		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			cfg->service_tax = atof(line);
		}
		fgets(line, BLL_MAX_LINE, fp);
		if(strlen(line)>0) {
			strip_newline(line, BLL_MAX_LINE);
			cfg->vat_tax = atof(line);
		}

		fclose(fp);
	} else {

		strcpy(cfg->store_name, "vizcreations-store");
		strcpy(cfg->store_reg_id, "000ABC123");
		strcpy(cfg->store_addr, "Gandhinagar, Hyderabad");
		strcpy(cfg->store_estd, "01-01-1970");

		cfg->service_tax = 0.0;
		cfg->vat_tax = 0.0;
	}
}

int save_bll_cfg(BLLCFG *cfg) { // Save to file bll-cfg.txt
	int ret = FALSE;
	char *filename = BLL_CFGFILE;

	FILE *fp;
	fp = fopen(filename, "w+");

	if(fp) {
		fprintf(fp, "%s\n%s\n%s\n%s\n%.2lf\n%.2lf\n",
				cfg->store_name,
				cfg->store_reg_id,
				cfg->store_addr,
				cfg->store_estd,
				cfg->service_tax,
				cfg->vat_tax
		);

		fclose(fp);
		ret = TRUE;
	}

	return ret;
}

void show_bll_cfg(BLL *Bll) {
	BLLCFG *cfg;
	cfg = &Bll->cfg;
	printf("Store configuration:\r\n");
	printf("====================\r\n");

	printf("Name: %s\r\nID: %s\r\nAddress: %s\r\nEstablished: %s\r\nService-tax: %.2lf\r\nVAT-tax: %.2lf\r\n",
			cfg->store_name, cfg->store_reg_id, cfg->store_addr, cfg->store_estd, cfg->service_tax, cfg->vat_tax);
}
