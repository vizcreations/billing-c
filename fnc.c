/**
* @abstract generic functions for c applications
*/

#include <stdlib.h>

#include "fnc.h"

void strip_newline(char *str, int len) {
	int i = 0;

	for(i=0; i<len; i++) {
		if(str[i]=='\n') {
			str[i]='\0';
			break;
		}
	}
}
