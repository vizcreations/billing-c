/* Billing in C lets you create inventory, orders for a store or shop */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "bll.h"

BLLITM *create_bll_invt(BLL *Bll) {
	BLLITM *itm;

	itm = (BLLITM *) malloc(sizeof(BLLITM));

	if(itm==NULL) {
		printf("Creation of inventory failed!\n");
		return NULL;
	}

	Bll->firstitem = Bll->curritem = itm;
	itm->next = NULL;
	return itm; // memory size
}

BLLITM *add_bll_item(BLL *Bll) {
	BLLITM *itm;

	if(Bll->firstitem==NULL) { // Fresh inventory
		itm = create_bll_invt(Bll);
		return itm;
	}

	itm = (BLLITM *) malloc(sizeof(BLLITM));

	Bll->curritem->next = itm; // Set the previous last's next item to this to maintain chain in memory
	Bll->curritem = itm; // Set BLL's curritem to this item
	itm->next = NULL; // Of course this is the last item..

	return itm;
}

int scan_bll_item(BLLITM *itm, char *line) {
	char *token;
	char delim[] = ";";
	int complete_flag = FALSE;

	token = strtok(line, delim);
	if(token != NULL) {
		itm->id = atol(token);
		token = strtok(NULL, delim);
		if(token != NULL) {
			strcpy(itm->name, token);
			token = strtok(NULL, delim);
			if(token != NULL) {
				strcpy(itm->desc, token);
				token = strtok(NULL, delim);
				if(token != NULL) {
					itm->in_stock = atoi(token);
					token = strtok(NULL, delim);
					if(token != NULL) {
						itm->weight = atof(token);
						token = strtok(NULL, delim);
						if(token != NULL) {
							itm->price = atof(token);
							token = strtok(NULL, delim);
							if(token != NULL) {
								itm->mrp = atof(token);
								complete_flag = TRUE;
							}
						}
					}
				}
			}
		}
	}

	return complete_flag;
}

int init_bll_invt(BLL *Bll) {
	const char *file = BLL_INVFILE;
	int itemcount = 0;
	char line[BLL_MAX_LINE];
	BLLITM *itm;

	FILE *fp;
	fp = fopen(file, "r");
	if(fp) {
		while((fgets(line, BLL_MAX_LINE, fp))!=NULL) {
			itm = add_bll_item(Bll);
			if(itm) {
				if((scan_bll_item(itm, line))==TRUE)
					itemcount++;
			}
		}
		fclose(fp);
	}

	Bll->itemcount = itemcount;
	return itemcount;
}

int save_bll_item(BLLITM *itm) {
	int ret = FALSE;
	const char *file = BLL_INVFILE;

	FILE *fp;
	time_t ticks;

	ticks = time(NULL); // Now
	fp = fopen(file, "a+");
	if(fp) {
		fprintf(fp, "%ld;%s;%s;%d;%.2lf;%.2lf;%.2lf\n",
					itm->id,
					itm->name,
					itm->desc,
					itm->in_stock,
					itm->weight,
					itm->price,
					itm->mrp);
		printf("Item saved to inventory..\n");
		ret = TRUE;
		fclose(fp);
	}
	return ret;
}

/**
* Write all inventory in memory to file fresh
*/
int save_bll_items(BLL *Bll) {
	int ret = FALSE;
	BLLITM *itm, *next;

	const char *filename = BLL_INVFILE;
	FILE *fp;

	fp = fopen(filename, "w");
	if(fp) {
		for(itm=Bll->firstitem; itm; itm=next) {
			fprintf(fp, "%ld;%s;%s;%d;%.2lf;%.2lf;%.2lf\n",
					itm->id,
					itm->name,
					itm->desc,
					itm->in_stock,
					itm->weight,
					itm->price,
					itm->mrp);
			next = itm->next;
		}
		fclose(fp);
		ret = TRUE;
	}
	return ret;
}

BLLITM *get_bll_item(BLL *Bll, long id) {
	BLLITM *itm, *next;
	itm = next = NULL;

	for(itm=Bll->firstitem; itm; itm=next) {
		if(itm->id == id)
			break; // We got the item we wanted..

		next = itm->next;
	}

	return itm;
}

void show_bll_invt(BLL *Bll) {
	int itemcount = 0;
	BLLITM *itm, *next;
	printf("Displaying items..\n");

	itemcount = Bll->itemcount;

	for(itm=Bll->firstitem; itm; itm=next) {
		printf("ID: %ld; Name: %s; Description: %s; In-stock: %d; Weight: %.2lf; Price: %.2lf; MRP: %.2lf\n",
				itm->id, itm->name, itm->desc, itm->in_stock, itm->weight, itm->price, itm->mrp);
		next = itm->next;
	}

	if(itemcount == 0)
		puts("No items in inventory..");
}

int bll_item_exists(BLL *Bll, long id) {
	int exists = FALSE; // Default
	BLLITM *item, *next;

	for(item=Bll->firstitem; item; item=next) {
		if(item->id==id) {
			exists = TRUE;
			break;
		}

		next = item->next;
	}

	return exists;
}

int del_bll_item(BLL *Bll, long id) {
	BLLITM *itm, *prev, *next;
	BLLITM *delitm = NULL;
	int ret = FALSE;

	for(itm=Bll->firstitem; itm; itm=next) {
		if(itm->id == id) {
			delitm = itm;
			break;
		} else prev = itm;
		next = itm->next;
	}

	// It wasn't the first item we deleted..
	if(prev!=NULL && delitm!=NULL) { // Chain previous and next items
		next = delitm->next;
		prev->next = next;

		if(delitm==Bll->curritem) // Last item deleted, so set up last item pointer
			Bll->curritem = prev;

		if(delitm==Bll->firstitem) // First item deleted, so set up first item pointer
			Bll->firstitem = next;

		free(delitm);
		ret = TRUE;
	}

	return ret;
}
