/* Header file for Billing in Linux */

#define BLL_MIN_INPUT 50
#define BLL_MAX_INPUT 255
#define BLL_ORDFILE "_data/orders.bab"
#define BLL_INVFILE "_data/invt.bab"

#define BLL_CFGFILE "bll.cfg"
#define BLL_MAX_LINE 1000
#define BLL_MAX_DATA 4500

#define BLL_FIRSTITEMID 10001
#define BLL_FIRSTORDERID 1001
#define BLL_DELIM ';' // Delimits the items in the 

enum {
	FALSE = 0,
	TRUE = 1
};

/** Struct definitions */
typedef struct BllItem {
	long id;
	char name[BLL_MIN_INPUT];
	char desc[BLL_MAX_INPUT];

	int in_stock;
	double price;
	double mrp;
	double weight;

	struct BllItem *next; // First in list
} BLLITM;

typedef struct BllCustomer {
	char name[BLL_MIN_INPUT];
	char phone[BLL_MIN_INPUT];
	char email[BLL_MIN_INPUT];
	char address[BLL_MAX_INPUT];
} BLLCUST;

typedef struct BllOrderItem {
	long id;
	struct BllOrderItem *next;
} BLLORDITM;

typedef struct BllOrder {
	long id;
	BLLCUST cust; // The customer of this order

//	long **itemids; // Pointer to array of long integers (word)
	BLLORDITM *firstorditem;
	BLLORDITM *currorditem;

	double total;
	long timestamp; // Order time/date
	struct BllOrder *next;
} BLLORD;

typedef struct BllConfig {
	char store_name[BLL_MIN_INPUT];
	char store_reg_id[BLL_MIN_INPUT];
	char store_addr[BLL_MAX_INPUT];
	char store_estd[BLL_MIN_INPUT];

	double service_tax;
	double vat_tax;
} BLLCFG;

typedef struct Billng {
	BLLCFG cfg; // Static retaining memory

	/** Inventory */
	BLLITM *firstitem; // Dynamically allocated memory
	BLLITM *curritem; // Usually the last one in list

	/** Orders */
	BLLORD *firstord;
	BLLORD *currord;

	int itemcount;
	int ordercount;
} BLL;

/** Function prototypes */
void init_bll_cfg(BLL *);
int init_bll_invt(BLL *);
int init_bll_ord(BLL *);

BLLITM *create_bll_invt(BLL *);
BLLITM *add_bll_item(BLL *);
int scan_bll_item(BLLITM *, char *);
int bll_item_exists(BLL*, long);
int del_bll_item(BLL *, long);

BLLORDITM *create_bll_forditem(BLLORD *);
BLLORDITM *add_bll_orditem(BLLORD *);

BLLORD *create_bll_ford(BLL *);
BLLORD *add_bll_ord(BLL *);
int scan_bll_ord(BLLORD *, char *);
int bll_ord_exists(long);
int del_bll_ord(BLL *, long);

int save_bll_cfg(BLLCFG *);
int save_bll_item(BLLITM *); // Save to file
int save_bll_items(BLL *);
int save_bll_ord(BLLORD *);
int save_bll_ords(BLL *);

BLLITM *get_bll_item(BLL *, long); // Pass id, Billing struct
BLLORD *get_bll_ord(BLL *, long);
BLLORDITM *get_bll_orditem(BLLORD *, long);

void del_bll_orditems(BLLORD *);

void show_bll_cfg(BLL *);
void show_bll_invt(BLL *);
void show_bll_ord(BLL *);
