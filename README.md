Billing and Inventory management
================================

Billing and Inventory is a command line C application designed
to create inventory, orders, invoices suitable for
any store or hotel and print them to paper


Usage:
======

Simply compile the source code using the Make
engine or use the pre-compiled binary. Here's a 
few commands to get you started..

1. :~$ ./Bll invt -s // Show the inventory
2. :~$ ./Bll invt -n 'name' 'description' 'weight' 'price' 'mrp'
3. :~$ ./Bll ord -s
4. :~$ ./Bll ord -n 'customer-name' 'item:id:s' // Colons separate the actual IDs
5. :~$ ./Bll ord -e 'order-id' 'customer-name' 'item:id:s' // Update
5. :~$ ./Bll cfg -s
6. :~$ ./Bll cfg -m 'store-name' 'store-id' 'store-address' 'store-established-date' service-tax vat-tax
7. :~$ ./Bll ord -d 'order-id' // Deletes an order


OpenSource:
===========

We at vizcreations have always wanted to write code and
let people decide if it's well written. They can use it
if they wanted to, however we have attached the GNU GPL 2.0
license to all our source code, hence you have to give away
the code to whoever you are selling or sharing with

Full manual will be coming soon..


vizcreations
vizcreations.com
http://facebook.com/vizcreations.page
