# Makefile for billing application

SRC_CIL = item.c order.c config.c fnc.c
OBJ_CIL = item.o order.o config.o fnc.o

CIL_INCLUDES = -I/usr/include -I/usr/local/include
CIL_LIBS = -L/usr/lib -L/usr/local/lib -lc -lm

all: cil_cl main_cl

cil_cl:	
	gcc -c $(SRC_CIL)
	ar rcs bll.a $(OBJ_CIL)
	$(RM) *.o

main_cl:
	$(RM) Bll
	gcc -o Bll $(CIL_INCLUDES) main.c bll.a $(CIL_LIBS) -g -Werror -Wall

clean:
	$(RM) *.o *.a Bll tmp/*
